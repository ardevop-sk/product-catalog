package sk.ardevop.productcatalog;

import java.util.Arrays;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import sk.ardevop.productcatalog.api.ProductsApi;
import sk.ardevop.productcatalog.model.ProductRto;

@RestController
public class ProductsController implements ProductsApi {

  @Override
  public ResponseEntity<List<ProductRto>> findProducts() {
    return ResponseEntity.ok(Arrays.asList(
        ProductRto.builder().id("1").name("test1").category("category1").price(15.50f).build(),
        ProductRto.builder().id("2").name("test2").category("category1").price(5.50f).build(),
        ProductRto.builder().id("3").name("test3").category("category2").price(20.50f).build()
    ));
  }
}
